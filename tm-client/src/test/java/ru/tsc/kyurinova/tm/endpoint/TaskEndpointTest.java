package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SkipCategory;

public class TaskEndpointTest {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String taskName = "taskTest";

    @NotNull
    private final String taskDescription = "taskTestDescription";

    @Nullable
    private String taskId;

    @NotNull
    private TaskDTO task;

    @NotNull
    private SessionDTO session;

    public TaskEndpointTest() {
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.createTaskDescr(session, taskName, taskDescription);
        taskId = taskEndpoint.findByNameTask(session, taskName).getId();
    }

    @Test
    @Category(SkipCategory.class)
    public void findAllTaskTest() {
        Assert.assertEquals(1, taskEndpoint.findAllTask(session).size());
    }

    @Test
    @Category(SkipCategory.class)
    public void findTaskTest() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, taskId));
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findByNameTask(session, taskName));
        Assert.assertNotNull(taskEndpoint.findByIndexTask(session, 0));
    }

    @Test
    @Category(SkipCategory.class)
    public void removeByIdTaskTest() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, taskId));
        taskEndpoint.removeByIdTask(session, taskId);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SkipCategory.class)
    public void removeByIndexTaskTest() {
        Assert.assertNotNull(taskEndpoint.findByIndexTask(session, 0));
        taskEndpoint.removeByIndexTask(session, 0);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SkipCategory.class)
    public void removeByNameTaskTest() {
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findByNameTask(session, taskName));
        taskEndpoint.removeByNameTask(session, taskName);
        Assert.assertTrue(taskEndpoint.findAllTask(session).isEmpty());
        taskId = null;
    }

    @Test
    @Category(SkipCategory.class)
    public void createTest() {
        taskEndpoint.createTaskDescr(session, "taskTestNew", "taskTestNew");
        @NotNull final String buffTaskId = taskEndpoint.findByNameTask(session, "taskTestNew").getId();
        Assert.assertNotNull(buffTaskId);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, buffTaskId));
        taskEndpoint.removeByIdTask(session, buffTaskId);
    }

    @Test
    @Category(SkipCategory.class)
    public void updateByIdTaskTest() {
        Assert.assertNotNull(taskId);
        @NotNull final String newProjectName = "newTaskName";
        @NotNull final String newProjectDescription = "newTaskDescription";
        taskEndpoint.updateByIdTask(session, taskId, newProjectName, newProjectDescription);
        @NotNull final TaskDTO updatedTask = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertEquals(taskId, updatedTask.getId());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
        Assert.assertEquals(newProjectName, updatedTask.getName());
        Assert.assertEquals(newProjectDescription, updatedTask.getDescription());
    }

    @Test
    @Category(SkipCategory.class)
    public void updateByIndexTaskTest() {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        taskEndpoint.updateByIndexTask(session, 0, newProjectName, newProjectDescription);
        @NotNull final TaskDTO updatedTask = taskEndpoint.findByIndexTask(session, 0);
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
        Assert.assertEquals(newProjectName, updatedTask.getName());
        Assert.assertEquals(newProjectDescription, updatedTask.getDescription());
    }

    @Test
    @Category(SkipCategory.class)
    public void startByIdTaskTest() {
        Assert.assertNotNull(taskId);
        taskEndpoint.startByIdTask(session, taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByIdTask(session, taskId).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void startByIndexTaskTest() {
        taskEndpoint.startByIndexTask(session, 0);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByIndexTask(session, 0).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void startByNameTaskTest() {
        Assert.assertNotNull(taskName);
        taskEndpoint.startByNameTask(session, taskName);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByNameTask(session, taskName).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void finishByIdTaskTest() {
        Assert.assertNotNull(taskId);
        taskEndpoint.finishByIdTask(session, taskId);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByIdTask(session, taskId).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void finishByIndexTaskTest() {
        taskEndpoint.finishByIndexTask(session, 0);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByIndexTask(session, 0).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void finishByNameTaskTest() {
        Assert.assertNotNull(taskName);
        taskEndpoint.finishByNameTask(session, taskName);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByNameTask(session, taskName).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void changeStatusByIdTaskTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(taskId);
        taskEndpoint.changeStatusByIdTask(session, taskId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.findByIdTask(session, taskId).getStatus());
        taskEndpoint.changeStatusByIdTask(session, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByIdTask(session, taskId).getStatus());
        taskEndpoint.changeStatusByIdTask(session, taskId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByIdTask(session, taskId).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void changeStatusByIndexTaskTest() {
        Assert.assertNotNull(session);
        taskEndpoint.changeStatusByIndexTask(session, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.findByIndexTask(session, 0).getStatus());
        taskEndpoint.changeStatusByIndexTask(session, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByIndexTask(session, 0).getStatus());
        taskEndpoint.changeStatusByIndexTask(session, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByIndexTask(session, 0).getStatus());
    }

    @Test
    @Category(SkipCategory.class)
    public void changeStatusByNameTaskTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(taskName);
        taskEndpoint.changeStatusByNameTask(session, taskName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.findByNameTask(session, taskName).getStatus());
        taskEndpoint.changeStatusByNameTask(session, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.findByNameTask(session, taskName).getStatus());
        taskEndpoint.changeStatusByNameTask(session, taskName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.findByNameTask(session, taskName).getStatus());
    }

    @After
    public void after() {
        taskEndpoint.clearTask(session);
        sessionEndpoint.closeSession(session);
    }

}
