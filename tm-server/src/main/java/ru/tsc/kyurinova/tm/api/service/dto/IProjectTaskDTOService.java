package ru.tsc.kyurinova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.List;

public interface IProjectTaskDTOService {

    void bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) throws SQLException;

    void removeById(@Nullable String userId, @Nullable String projectId) throws SQLException;

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) throws SQLException;

}
